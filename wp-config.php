<?php
define('WP_AUTO_UPDATE_CORE', 'minor');// This setting is required to make sure that WordPress updates can be properly managed in WordPress Toolkit. Remove this line if this WordPress website is not managed by WordPress Toolkit anymore.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'johnsonrenovations');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'pighsts');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$Y-r?(Cm1o?r8PEO{]!+B`L29I#G9S7xP47~9|lg_XEo:Iov)DP_u%gT9Clf_ WP');
define('SECURE_AUTH_KEY',  'p2(p547v(EI[]b9T*>L fQ7o;jKS;+vg!tCj+3 p0+8,__r/@8vIg%ZH6BLMD2:^');
define('LOGGED_IN_KEY',    'Xk23bX8r7L)a7uruQDZbt8r{j_:YLEB: 44.t(bk1xN.Kv{~|sTXtfxMY>&,pv{N');
define('NONCE_KEY',        'ACi`m!W*3E~`nt|XjzE(**z7<AM*=4Zx3mP-g:{|6O= PW2$zJn;YVBb/8$^8_dE');
define('AUTH_SALT',        ':wZc4-q(e1Y1Z+[+9EO8~03PB3Fe}.phmoH`iB{*P_D6V3zVkISZ+4^;<3,yfmcZ');
define('SECURE_AUTH_SALT', '/K@6[bTy]EmCB2:cHj B2z@7Omx]Vm|wc8i2T?,7$dAh!-g00S(ee;Z1m,pJzexY');
define('LOGGED_IN_SALT',   'y~ySoo!T*u~!_cr-#8ur5EfAND0fj&xW`0bMSPg=iLi>!-QFlh^}-0qSOjqgQk/V');
define('NONCE_SALT',       '3`E|axR865g<8BRs.%a[q^q956C0Frg1=39FN*V(WhEpE)j2,;?p%gaRD%koy9]F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_HOME', 'http://oscar.local/johnsonrenovations' );
define( 'WP_SITEURL', 'http://oscar.local/johnsonrenovations' );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
