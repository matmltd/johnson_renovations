��          \      �       �      �   2   �        _     k   s     �  .   �  )  ,     V  2   ]     �  _   �  k         l  4   �                                       Google Lazy-loads media using the native browser feature. Native Lazyload Native Lazyload requires at least PHP version %1$s. Your site is currently running on PHP %2$s. Native Lazyload requires at least WordPress version %1$s. Your site is currently running on WordPress %2$s. https://opensource.google.com https://wordpress.org/plugins/native-lazyload/ PO-Revision-Date: 2019-09-08 16:36:42+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: en_GB
Project-Id-Version: Plugins - Native Lazyload - Stable (latest release)
 Google Lazy loads media using the native browser feature. Native Lazyload Native Lazyload requires at least PHP version %1$s. Your site is currently running on PHP %2$s. Native Lazyload requires at least WordPress version %1$s. Your site is currently running on WordPress %2$s. https://opensource.google.com https://en-gb.wordpress.org/plugins/native-lazyload/ 