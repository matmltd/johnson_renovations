<?php
/**
 * Plugin Name: MATM Branding
 * Plugin URI: https://matm.co.uk
 * Description: Adds simple branding to the wordpress admin section to avoid confusion
 * Version: 1.0
 * Author: Jacob Tilsley-Curtis
 * Author URI: https://matm.co.uk
 */
 
 /* Login */
 function matm_login_logo() { 
?> 
<style type="text/css"> 
	body.login div#login h1 a {
		background-image: url('<?=plugin_dir_url(__FILE__)?>/imgs/matm-logo.png');
		background-image: url('<?=plugin_dir_url(__FILE__)?>/imgs/matm-logo.svg');
		padding-bottom: 30px;     
		width: 200px;
	    background-size: contain;
	} 
	p.message {
    	color: black;
	}
	body.login {
    	background-color: #33414b;
    	color: white !important;
	}

body.login form {
    background-color: #33414a !important;
    box-shadow: unset;
}

.login label {
    color: white !important;
}

.login #backtoblog a, .login #nav a {
    color: white !important;
}
</style>
 <?php 
} 
add_action( 'login_enqueue_scripts', 'matm_login_logo' );

function matm_login_logo_url($url) {
     return "https://matm.co.uk";

}
add_filter( 'login_headerurl', 'matm_login_logo_url' );

/* Dashboard */

function wpb_custom_logo() {
echo "<style type='text/css'>
	#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
		background-image: url(" . plugin_dir_url(__FILE__) . "/imgs/matm-small@3x.png) !important;
		background-image: url(" . plugin_dir_url(__FILE__) . "/imgs/matm-small.svg) !important;
    	background-position: 0 0;
	    color: rgba(0, 0, 0, 0);
    	background-size: contain;
	    background-repeat: no-repeat;
    	width: 100%;
	    background-position: center !important;
    }
	#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
		background-position: 0 0;
		color:rgba(0, 0, 0, 0);
	}
	#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
		background-position: 0 0;
	}
</style>";
}
 
//hook into the administrative header output
add_action('wp_before_admin_bar_render', 'wpb_custom_logo');