<?php

//  ATTENTION!
//
//  DO NOT MODIFY THIS FILE BECAUSE IT WAS GENERATED AUTOMATICALLY,
//  SO ALL YOUR CHANGES WILL BE LOST THE NEXT TIME THE FILE IS GENERATED.
//  IF YOU REQUIRE TO APPLY CUSTOM MODIFICATIONS, PERFORM THEM IN THE FOLLOWING FILE:
//  /var/www/vhosts/johnsonrenovations.co.uk/httpdocs/wp-content/maintenance/template.phtml


$protocol = $_SERVER['SERVER_PROTOCOL'];
if ('HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol) {
    $protocol = 'HTTP/1.0';
}

header("{$protocol} 503 Service Unavailable", true, 503);
header('Content-Type: text/html; charset=utf-8');
header('Retry-After: 600');
?>

<html>
<head>
<meta charset="UTF-8">

<title>     Johnson Renovations    </title>
<style type="text/css">
body {
	background-color: #00537d;
}

a:any-link {
	color:#ffffff !important;
	text-decoration:none;
}

a:link {
    color:#ffffff !important;
    text-decoration:none;
}


</style>
</head>

<body>
<table width="100%" border="0" cellpadding="20" >
  <tbody>
    <tr>
      <td height="400" colspan="5" align="center"><img src="http://johnsonrenovations.co.uk/wp-content/maintenance/assets/images/logo.svg" alt="" width="300px"/>
      
</td>
    </tr>
    <tr>
    <td height="100" colspan="5"align="center" style="font-family: 'Signika', sans-serif;
font-weight: 700; font-size:22px; color:#fff;">
    Please bear with us whilst we update a few important files on our website
 
<br><br>We’ll be back up and running very soon 
    </td>
    </tr>
    <tr>
    
    <td height="200" width="35%" align="center" style="font-family: 'Signika', sans-serif;
font-weight: 700; font-size:20px; color:#fff;">
    </td>
        <td height="200" width="35%" align="center" style="font-family: 'Signika', sans-serif;
font-weight: 700; font-size:20px; color:#fff;">
    Phone: 0845 457 0000
    <br><br>Email: <a href="mailto:hire@jms.co.uk">training@jms.co.uk</a>
    </td>
        <td height="200" width="35%" align="center" style="font-family: 'Signika', sans-serif;
font-weight: 700; font-size:20px; color:#fff;">
    </td>
    <td align="center" style=" width:30%;" >    
    </td>
    
    </tr>
  </tbody>
</table>
</body>
</html>
