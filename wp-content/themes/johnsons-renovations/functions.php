<?php

wp_enqueue_script('child-scripts', get_stylesheet_directory_uri() . "/scripts.js", array( 'jquery' ));

add_shortcode('date', 'show_the_date');

function show_the_date() {
	return date('Y');
}

add_image_size('homepage_thumbnail', 320, 190, true);