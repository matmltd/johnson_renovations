<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package buildpro
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65232343-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-65232343-1');
</script>

<?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?> >

    <div id="wrapper" class="<?php if( buildpro_get_option('header_layout') == '4' ) echo 'side-content'; ?>">
        <!-- header begin -->
        <?php if( buildpro_get_option('header_layout') == '2' ){ ?>
        <header class="header-solid">
        <?php }elseif( buildpro_get_option('header_layout') == '3' ){ ?>
        <header class="header-solid header-light">
        <?php }elseif( buildpro_get_option('header_layout') == '4' ){ ?>
        <header class="transparent de_header_2 clone">
        <?php }else{ ?>
        <header class="">
        <?php } ?>

            <?php if(buildpro_get_option( 'top_header' )) { ?>
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <!-- logo begin -->
                        <h1 id="logo">
                            <a href="<?php echo esc_url( home_url('/') ); ?>">
                                <img class="logo" src="http://johnsonrenovations.co.uk/wp-content/themes/johnsons-renovations/images/logo.svg" class="img-responsive" alt="">
                                <img class="logo-2" src="http://johnsonrenovations.co.uk/wp-content/themes/johnsons-renovations/images/logo.svg" class="img-responsive" alt="">
                            </a>                            
                        </h1>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav class="mainmenu">
                            <?php

                                $primary = array(
                                    'theme_location'  => 'primary',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => 'buildpro_bootstrap_navwalker::fallback',
                                    'walker'          => new buildpro_bootstrap_navwalker(),
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul id="mainmenu" class="'.buildpro_get_option("sepe_list").'">%3$s</ul>',
                                    'depth'           => 0,
                                );
                                if ( has_nav_menu( 'primary' ) ) {
                                    wp_nav_menu( $primary );
                                }
                            ?>    
                        </nav>

                    </div>
                    <!-- mainmenu close -->
                        </div>
                </div>
            </div>
            <?php } ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        

                </div>
            </div>
        </header>
        <div id="contents" class="no-bottom no-top">